import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/lib/integration/react'
import createStore from 'App/Stores'
import RootScreen from './Containers/Root/RootScreen'
import {
  ApolloClient,
  ApolloProvider,
  createHttpLink,
  InMemoryCache
} from '@apollo/client'
import { Config } from 'App/Config'

const { store, persistor } = createStore()


// configuring our API URI (within the readme) & cache 
const uri = 'https://graphqlzero.almansi.me/';
const cache = new InMemoryCache();


// configuring Apollo Client
const client = new ApolloClient({
  uri,
  cache,
  queryDeduplication: false,
    defaultOptions: {
      watchQuery: {
        fetchPolicy: 'cache-and-network',
      },
    }
});


export default class App extends Component {
  render() {
    return (
      /** * @see https://github.com/reduxjs/react-redux/blob/master/docs/api/Provider.md */
      <ApolloProvider client={client}>
        <Provider store={store}>
          {/**
           * PersistGate delays the rendering of the app's UI until the persisted state has been retrieved
           * and saved to redux.
           * The `loading` prop can be `null` or any react instance to show during loading (e.g. a splash screen),
           * for example `loading={<SplashScreen />}`.
           * @see https://github.com/rt2zz/redux-persist/blob/master/docs/PersistGate.md
          */}
          <PersistGate loading={null} persistor={persistor}>
            <RootScreen />
            </PersistGate>
          </Provider>
       </ApolloProvider>
    )
  }
}
