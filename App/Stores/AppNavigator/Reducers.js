/**
 * Reducers specify how the application's state changes in response to actions sent to the store.
 *
 * @see https://redux.js.org/basics/reducers
 */

import { INITIAL_STATE } from './InitialState'
import { createReducer } from 'reduxsauce'
import { AppNavigatorTypes } from './Actions'

export const navigationLoaded = (state) => ({
  ...state,
  navigationIsLoaded: true,
})

/**
 * @see https://github.com/infinitered/reduxsauce#createreducer
 */
export const reducer = createReducer(INITIAL_STATE, {
  [AppNavigatorTypes.NAVIGATION_LOADED]: navigationLoaded,
})
