import { Text, View, ActivityIndicator } from 'react-native';
import { gql, useQuery } from '@apollo/client'
import React from 'react';
import User from '../../Components/Users';


  // our GraphQL query for the list of users
const GET_USERS = gql`
        query users {
            users {
                data {
                id,
                name,
                username,
                todos {
                data {
                    id,
                    title,
                    completed
                }
                }
            }
            }
        }
`;

const UsersScreen = (props) => {
    const { loading, error, data } = useQuery(GET_USERS);
  
    // if the data is loading, our app will display a loading message
    if (loading) return <ActivityIndicator size="large"/>;

    // if there is an error fetching the data, display an error message
    if (error) {
        console.log(error);
        return (
            <Text>
              {error.graphQLErrors.map(({ message }, i) => (
                <Text key={i}>error : {message}</Text>
              ))}
            </Text>
          )
    }
    // if the query is successful and there are users, return the list of users
    // else if the query is successful and there aren't users, display a message
    if (data.users.data.length !== 0) {
        console.log(data.users.data);
        return (
            <View>
              {data.users.data.map((user) => (
                <User key={user.id} user={user} />
              ))}
            </View>
          )
    } else {
         return <Text>No users found</Text>;
    }
};

 
export default UsersScreen;