import React, { useLayoutEffect, useState } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import NavigationService from 'App/Services/NavigationService'

import SplashScreen from 'App/Containers/SplashScreen/SplashScreen'
import UsersScreen from '../Containers/users/UsersScreen'
import UserDetails from '../Containers/UserDetails/UserDetailsScreen'

/**
 * The root screen contains the application's navigation.
 *
 * @see https://reactnavigation.org/docs/en/hello-react-navigation.html#creating-a-stack-navigator
 */

const Stack = createStackNavigator();

const StackNavigator = () => {
  return(
    <Stack.Navigator>
      <Stack.Screen name="Users" component={UsersScreen} />
      <Stack.Screen name="UsersDetails" component={UserDetails} />
    </Stack.Navigator>
  )
}

export default function MyStack() {
  const [isMounted, setIsMounted] = useState(false)

  useLayoutEffect(() => {
    setIsMounted(true)

    return () => {
      setIsMounted(false)
    }
  }, [])

  if (!isMounted) return <SplashScreen />

  return (
    <NavigationContainer
      // Initialize the NavigationService
      ref={(navigatorRef) => {
        NavigationService.setTopLevelNavigator(navigatorRef)
      }}
    >
      <StackNavigator/>
    </NavigationContainer>
  )
}
