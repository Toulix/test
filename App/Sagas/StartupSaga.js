import { put, take } from 'redux-saga/effects'
import NavigationService from 'App/Services/NavigationService'
import ExampleActions from 'App/Stores/Example/Actions'
import { AppNavigatorTypes } from 'App/Stores/AppNavigator/Actions'

/**
 * The startup saga is the place to define behavior to execute when the application starts.
 */
export function* startup() {
  // Dispatch a redux action using `put()`
  // @see https://redux-saga.js.org/docs/basics/DispatchingActions.html
  yield put(ExampleActions.fetchUser())

  // Wait for our navigation provider to be loaded
  yield take(AppNavigatorTypes.NAVIGATION_LOADED)

  // Add more operations you need to do at startup here
  // ...

  // When those operations are finished we redirect to the main screen
  NavigationService.navigateAndReset('Example')
}
